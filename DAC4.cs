﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using UEI.QuickSet.DAC3aLib;

namespace DACReadWriter.DACReader
{
    public partial class Dac4 : Form
    {
        string _filepath = string.Empty;
        DACWriter.DAC4Version _version;
        public DACWriter.DAC4Version Version
        {
            get { return _version; }
            set { _version = value; }
        }
        public string Filepath
        {
            get { return _filepath; }
            set { _filepath = value; }
        }
        DAC4ReaderLib _dac4reader;
        string _menuoptionselected = string.Empty;
        public Dac4(string menuoption)
        {
            InitializeComponent();
            _menuoptionselected = menuoption;
        }

        private void button1_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void comboBox1_SelectedIndexChanged(object sender, EventArgs e)
        {
            //LoadDevices(comboBox1.SelectedItem.ToString());
            //label2.Text = _dac4reader._device;
            if (Version==DACWriter.DAC4Version.two)
            {
                _dac4reader.GetAllFingerprintdetails(comboBox1.SelectedItem.ToString());
                _dac4reader.GetFPInfo(comboBox1.SelectedItem.ToString());
                dataGridView1.ClearSelection();
                dataGridView1.DataSource = _dac4reader._dt;
                label2.Text = _dac4reader._device;
            }
            else if (Version==DACWriter.DAC4Version.three)
            {
                _dac4readerv3.GetAllFingerprintdetails(comboBox1.SelectedItem.ToString());
                _dac4readerv3.GetFPInfo(comboBox1.SelectedItem.ToString());
                dataGridView1.ClearSelection();
                dataGridView1.DataSource = _dac4readerv3._dt;
                label2.Text = _dac4readerv3._device;
            }
        }

        private void comboBox2_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void comboBox3_SelectedIndexChanged(object sender, EventArgs e)
        {

        }

        private void LoadFingerprintsV3()
        {
            _dac4readerv3.GetAllBrands();
            _dac4readerv3.ReadFileToc();
            _dac4readerv3.GetAllFingerPrintDetails();
            List<string> fplist = new List<string>();
            foreach (KeyValuePair<string, int> dp in _dac4readerv3.fpaddress)
            {
                fplist.Add(dp.Key);
            }
            comboBox1.DataSource = fplist;
            //label2.Text = _dac4reader._device;
        }
       

        private void LoadFingerprints()
        {
            _dac4reader.GetAllBrands();
            _dac4reader.ReadFileToc();
            _dac4reader.GetAllFingerPrintDetails();
            List<string> fplist = new List<string>();
            foreach (KeyValuePair<string, int> dp in _dac4reader.fpaddress)
            {
                fplist.Add(dp.Key);
            }
            comboBox1.DataSource = fplist;
            //label2.Text = _dac4reader._device;
        }

        private void LoadDevicesV3(string fingerprint)
        {
            _dac4readerv3.GetDevices(comboBox1.SelectedItem.ToString());
            label3.Text = _dac4readerv3._version;
        }

        private void LoadDevices(string fingerprint)
        {
            _dac4reader.GetDevices(comboBox1.SelectedItem.ToString());
            label3.Text = _dac4reader._version;
        }

        private void LoadBrands(string fingerprint, string device)
        {

        }

        private void DAC4V2Reader()
        {
            if (_menuoptionselected == "SingleSearch")
            {
                _dac4reader = new DAC4ReaderLib();
                _dac4reader.ReadDAC3a(Filepath);

                LoadFingerprints();
                label3.Text = _dac4reader._version;
            }
            else if (_menuoptionselected == "BrandList")
            {
                _dac4reader = new DAC4ReaderLib();

                _dac4reader.Filepath = _filepath;
                _dac4reader.ReadDAC3a(_filepath);
                string _type = _dac4reader.GetDACType();
                _dac4reader.GetAllBrands();
                comboBox1.Enabled = false;

                dataGridView1.ClearSelection();
                dataGridView1.Columns.Add("Brands", "Brands");

                foreach (string brand in _dac4reader._brands)
                {
                    string[] _data = { brand };
                    dataGridView1.Rows.Add(_data);
                }
                label3.Text = _dac4reader._version;

            }
            else if (_menuoptionselected == "IpDiscoveryData")
            {
                _dac4reader = new DAC4ReaderLib();

                _dac4reader.Filepath = _filepath;
                _dac4reader.ReadDAC3a(_filepath);
                string _type = _dac4reader.GetDACType();
                _dac4reader.GetAllBrands();
                comboBox1.Enabled = false;

                dataGridView1.ClearSelection();
                _dac4reader.GetAllBrands();
                _dac4reader.ReadFileToc();
                _dac4reader.GetAllIpData();
                dataGridView1.DataSource = _dac4reader.IpDiscoverData;
                label3.Text = _dac4reader._version;
            }
            else if (_menuoptionselected == "CECDiscoveryData")
            {
                _dac4reader = new DAC4ReaderLib();

                _dac4reader.Filepath = _filepath;
                _dac4reader.ReadDAC3a(_filepath);
                string _type = _dac4reader.GetDACType();
                _dac4reader.GetAllBrands();
                comboBox1.Enabled = false;

                dataGridView1.ClearSelection();
                _dac4reader.GetAllBrands();
                _dac4reader.ReadFileToc();
                _dac4reader.GetAllCecData();
                dataGridView1.DataSource = _dac4reader.CecDiscoverdata;
                label3.Text = _dac4reader._version;
            }

            else if (_menuoptionselected == "InfoFrameData")
            {
                _dac4reader = new DAC4ReaderLib();

                _dac4reader.Filepath = _filepath;
                _dac4reader.ReadDAC3a(_filepath);
                string _type = _dac4reader.GetDACType();
                _dac4reader.GetAllBrands();
                comboBox1.Enabled = false;

                dataGridView1.ClearSelection();
                _dac4reader.GetAllBrands();
                _dac4reader.ReadFileToc();
                _dac4reader.GetAllInfoFrameData();
                dataGridView1.DataSource = _dac4reader.Infoframedata;
                label3.Text = _dac4reader._version;
            }
        }

        DAC4ReaderV3 _dac4readerv3;
        private void DAC4V3Reader()
        {
            _dac4readerv3 = new DAC4ReaderV3();
            if (_menuoptionselected == "SingleSearch")
            {
                _dac4readerv3 = new DAC4ReaderV3();
                _dac4readerv3.ReadDAC4(Filepath);

                LoadFingerprintsV3();
                //label3.Text = _dac4reader._version;
            }
        }

        private void SingleSearch_Load(object sender, EventArgs e)
        {

            switch (Version)
            {
                case DACWriter.DAC4Version.two:
                    {
                        DAC4V2Reader();
                        break;
                    }
                case DACWriter.DAC4Version.three:
                    {
                        DAC4V3Reader();
                        break;
                    }
            }
            

        }


    }
}
