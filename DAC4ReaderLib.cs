﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using UEI.QuickSet.DAC3aLib;
using UEI.QuickSet.DAC9a;
using System.Data;

namespace DACReadWriter.DACReader
{
    public class DAC4ReaderLib
    {
        
        byte[] Dac3a;
        Dictionary<int, byte> _dacMemoryMap = new Dictionary<int, byte>();

        string _filepath;
        public string Filepath
        {
            get { return _filepath; }
            set { _filepath = value; }
        }
        
        public void ReadDAC3a(string Filepath)
        {
            Dac3a = System.IO.File.ReadAllBytes(Filepath);
            _dacMemoryMap = new Dictionary<int, byte>();

            for (int i = 0; i < Dac3a.Length; i++)
            {
                _dacMemoryMap.Add(i, Dac3a[i]);
            }
        }

        public bool ValidateDAC()
        {
            bool _status = false;
            string _signature = Conversion.ConvertByteArrayToUTF8(GetData(4, 0));
            if (_signature == "DAC4")
                _status = true;
            return _status;
        }

        int _startAddress = 0;
        int _nextAddress = 0;

        int _sizeofFileHeader = 12;
        int _sizeofFileToc = 18;
        int _sizeofBrandRecordHeader = 8;
        public string _version=string.Empty;
        public void ReadFileHeader()
        {
            _startAddress = _nextAddress;
            int _filetypeid = 4;
            _version = Conversion.ConvertByteToString(GetData(4, 4));
            int _fileversion = 4;
            int _fileflags = 4;

            byte[] typeid = new byte[_filetypeid];
            string _text1 = string.Empty;
            for (int i = _startAddress; i < _filetypeid; i++)
            {
                typeid[i] = Dac3a[i];
            }

            _text1 = Encoding.ASCII.GetString(typeid);
            _startAddress += _filetypeid;
            typeid = new byte[_fileversion];
            int counter = 0;
            for (int i = _startAddress; i < (_startAddress + _fileversion); i++)
            {
                typeid[counter] = Dac3a[i];
                counter++;
            }
            string _text2 = Encoding.ASCII.GetString(typeid);



        }

        public string GetDACType()
        {
            string _type = string.Empty;
            byte[] typeid = new byte[4];
            for (int i = 0; i < 4; i++)
            {
                typeid[i] = Dac3a[i];
            }
            _type = Encoding.ASCII.GetString(typeid);
            return _type;
        }

        Dictionary<string, Int32> _toc = new Dictionary<string, Int32>();
        public void ReadFileToc()
        {
            _toc = new Dictionary<string, Int32>();
            _startAddress = 12;
            string toc = string.Empty;
            Int32 adr = 0;

            toc = Conversion.ConvertByteToString(GetData(2, _startAddress));
            _nextAddress = _startAddress;
            _nextAddress += 2;
            Int32 size = Conversion.ConvertByteArrayToInt32(GetData(4, _nextAddress));

            _nextAddress += 4;

            int numberoffields = size / 6;

            for (int i = 0; i < numberoffields; i++)
            {
                toc = Conversion.ConvertByteToString(GetData(2, _nextAddress));
                _nextAddress += 2;
                adr = Conversion.ConvertByteArrayToInt32(GetData(4, _nextAddress));
                _nextAddress += 4;

                _toc.Add(toc, adr);
            }


        }
        public List<string> _brands = new List<string>();
        public void GetAllBrands()
        {
            _brands = new List<string>();
            _nextAddress = _sizeofFileHeader + 6+_sizeofFileToc;
            _nextAddress += 2;
            byte[] byteSizeOfBrands = new byte[4];
            //byteSizeOfBrands= GetData(4, _nextAddress);
            //byteSizeOfBrands = ConvertLittleEndianToByte(byteSizeOfBrands);
            Int32 sizeofbrandrec = Conversion.ConvertByteArrayToInt32(GetData(4, _nextAddress));
            _nextAddress += 4;
            int _numberofBrands = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
            _nextAddress += 2;
            for (int i = 0; i < _numberofBrands; i++)
            {
                int _size = Conversion.ConvertBytetoInt(_dacMemoryMap[_nextAddress]);
                _nextAddress++;
                string _br = Conversion.ConvertByteToString(GetData(_size, _nextAddress));
                _brands.Add(_br);
                _nextAddress += _size;
            }

            
            
        }
        public Dictionary<string, int> fpaddress = new Dictionary<string, int>();
        public void GetAllFingerPrintDetails()
        {
            fpaddress = new Dictionary<string, int>();
            _nextAddress = _sizeofFileHeader + 6+_sizeofFileToc;
            _nextAddress += 2;
            int sizeofbrandrec = Conversion.ConvertByteArrayToInt32(GetData(4, _nextAddress));
            _nextAddress += 4;
            int _numberofBrands = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
            _nextAddress += 2;

            _nextAddress += sizeofbrandrec;

            //Size of ARDRecord Header
            _nextAddress += 2;

            ARDRecordSize = Conversion.ConvertByteArrayToInt32(GetData(4, _nextAddress));
            _nextAddress += 4;

            LastArdAddress = _nextAddress + ARDRecordSize;

            //Size of ARDTocHeader
            _nextAddress += 2;

            ARDRecordTOCHeaderSize = Conversion.ConvertByteArrayToInt32(GetData(4, _nextAddress));
            _nextAddress += 4;

            int _sizeofARDDataCount = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
            _nextAddress += 2;
            for (int i = 0; i < _sizeofARDDataCount; i++)
            {
                byte[] tempdata = GetData(4, _nextAddress);
                string key = GetByteToString(tempdata);
                _nextAddress += 4;
                int _offset = Conversion.ConvertByteArrayToInt32(GetData(4, _nextAddress));
                _nextAddress += 4;

                fpaddress.Add(key, _offset);
            }



        }

        List<string> devices = new List<string>();
        public string _device = string.Empty;
        public Dictionary<string, int> fpsizeoffsets = new Dictionary<string, int>();
        private void GetSizedifferential()
        {
            fpsizeoffsets = new Dictionary<string, int>();
            
        }
        public void GetDevices(string fingerprint)
        {
            GetAllBrands();
            devices = new List<string>();
            foreach (KeyValuePair<string, int> adr in fpaddress)
            {
                if (adr.Key == fingerprint)
                {
                    _nextAddress = adr.Value;

                    break;
                }
            }
        }

        public byte[] ConvertLittleEndianToByte(byte[] data)
        {
            byte[] _bytedata = new byte[data.Length];
            int counter = 0;
            for (int i = data.Length - 1; i >= 0; i--)
            {
                _bytedata[counter] = data[i];
                counter++;
            }

            return _bytedata;
        }
        public byte[] GetData(int sizeofbyte, int startaddress)
        {
            byte[] _data = new byte[sizeofbyte];
            int _len = sizeofbyte - 1;
            try
            {
                for (int i = 0; i < _data.Length; i++)
                {
                    _data[i] = _dacMemoryMap[startaddress];
                    startaddress++;

                }

            }
            catch (Exception ex)
            {
                throw new Exception(ex.Message);
            }
            return _data;

        }
        public string GetByteToString(byte[] data)
        {
            string _data = string.Empty;
            Int32 _ard = Conversion.ConvertByteArrayToInt32(data);

            _data = "0x" + _ard.ToString("X");
            
            //_data = "0x";
            //int len = data.Length - 1;
            //for (int i = len; i >= 0; i--)
            //{
            //    //int dt = Convert.ToInt16(data[i].ToString(), 16);
            //    _data += data[i].ToString("X");
            //}

            return _data;
        }

        List<int> _difsize = new List<int>();

       

        public void GetAllFingerprintdetails(string fingerprint)
        {
            int _prevaddr = 0;
            int _difference = 0;

            int counter = 0;
            foreach (KeyValuePair<string, int> adr in fpaddress)
            {
                if (counter == 0)
                {
                    _prevaddr = adr.Value;
                }
                else if(counter<=fpaddress.Count)
                {
                    _difference = adr.Value - _prevaddr;
                    _difsize.Add(_difference);
                    _prevaddr = adr.Value;
                }
                else if (counter> fpaddress.Count)
                {
                    _difference = (LastArdAddress - 1) - _prevaddr;
                    _difsize.Add(_difference);
                }
                counter++;

            }
            _difference = (LastArdAddress - 1) - _prevaddr;
            _difsize.Add(_difference);
        }

        public DataTable _dt;
        public void GetFPInfo(string fingerprint)
        {
            _dt = new DataTable();
            SetDataTable();
            GetAllBrands();
            int totalSize = 0;
            int _startAdr = 0;
            int _endAdr = 0;
            bool found = false;
            for (int i = 0; i < fpaddress.Count; i++)
            {
                foreach (KeyValuePair<string, int> adr in fpaddress)
                {
                    if (adr.Key == fingerprint)
                    {
                        if (adr.Key == fingerprint)
                        {
                            _nextAddress = adr.Value;
                            _startAdr = adr.Value;
                            totalSize = adr.Value + _difsize[i];
                            _endAdr = totalSize;
                            found = true;
                            break;
                        }
                    }
                }
                if (found)
                    break;
            }

            StringBuilder _log = new StringBuilder();

            string _devicename = string.Empty;
            int sizeofentry = 0;
            string _brandName = string.Empty;
            int _keyMapCount = 0;

                 int size = 0;
                 int _ueifnId = 0;//2 bytes
                 int cecdatasize = 0; //2 bytes
                 int cecpriority = 0;//1 byte
                 int ceccmdcount = 0;// 1 byte
                 List<int> ceccmds = new List<int>();//n bytes
                 int irdatasize = 0;//1 byte
                 int irpriority = 0;//1 byte
                 int ircodesetcount = 0;//1 byte
                 List<string> ircodesets = new List<string>();//n bytes
                 int timings = 0;//2 byte
                 int ipdatasize = 0;//1 byte
                 byte _tempdata = new byte();

            if (_startAdr > 0 && _endAdr > 0)
            {
                
                _nextAddress = _startAdr;
                 _device = Conversion.ConvertByteToString(GetData(1, _nextAddress));
                _nextAddress += 1;
                 
                sizeofentry = Conversion.ConvertByteArrayToInt32(GetData(3, _nextAddress));
                 _nextAddress += 3;
                
                int index = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
                 _brandName = _brands[index];
                 _nextAddress += 2;

                 _nextAddress += 2;//Irmacro
                 _nextAddress += 2;//Power on
                 _nextAddress += 2;//Reserved
                 _keyMapCount = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
                 _nextAddress += 2;
                 
                 int keymapcounter = 0;
                 while (keymapcounter < _keyMapCount)
                 {
                     size = 0;
                     _ueifnId = 0;//2 bytes
                     cecdatasize = 0; //2 bytes
                      cecpriority = 0;//1 byte
                      ceccmdcount = 0;// 1 byte
                      ceccmds = new List<int>();//n bytes
                      irdatasize = 0;//1 byte
                      irpriority = 0;//1 byte
                      ircodesetcount = 0;//1 byte
                      ircodesets = new List<string>();//n bytes
                      timings = 0;//2 bytes
                      ipdatasize = 0;//1 byte
                      _tempdata = new byte();

                     _ueifnId = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
                     _log.AppendLine();
                     _log.AppendLine();
                     _log.AppendLine(string.Format("ueifnid:{0}, Address:{1}", _ueifnId.ToString(), _nextAddress.ToString()));
                     _nextAddress += 2;
                     size += 2;
                     

                     cecdatasize = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
                     _log.AppendLine(string.Format("cecdatasize:{0}, Address:{1}", cecdatasize.ToString(), _nextAddress.ToString()));
                     _nextAddress += 2;
                     size += 2;
                     
                     _dacMemoryMap.TryGetValue(_nextAddress,out _tempdata);
                     cecpriority = Conversion.ConvertBytetoInt(_tempdata);
                     _log.AppendLine(string.Format("cecpriority:{0}, Address:{1}", cecpriority.ToString(), _nextAddress.ToString()));
                     _nextAddress++;
                     size++;

                     

                     string _test = _ueifnId.ToString() + cecdatasize.ToString() + cecpriority.ToString();
                     if(cecpriority>0 )
                     {
                         _dacMemoryMap.TryGetValue(_nextAddress, out _tempdata);
                         ceccmdcount = Conversion.ConvertBytetoInt(_tempdata);
                         _log.AppendLine(string.Format("ceccmdcount:{0}, Address:{1}", ceccmdcount.ToString(), _nextAddress.ToString()));
                         //ceccmdcount = Conversion.ConvertByteArrayToInt16(GetData(1, _nextAddress));
                         _nextAddress++;
                         size++;
                         string _ceccmds = string.Empty;
                         string _cmdstartaddress = _nextAddress.ToString();
                         for (int i = 0; i < ceccmdcount; i++)
                         {
                             _dacMemoryMap.TryGetValue(_nextAddress, out _tempdata);
                             int cmdsize = Conversion.ConvertBytetoInt(_tempdata);
                             _nextAddress++;
                             size++;
                             ceccmds.Add(cmdsize);
                             _ceccmds += cmdsize.ToString() + ",";
                             for (int j = 0; j < cmdsize; j++)
                             {
                                 _dacMemoryMap.TryGetValue(_nextAddress, out _tempdata);
                                 ceccmds.Add(Conversion.ConvertBytetoInt(_tempdata));
                                 _ceccmds += _tempdata.ToString() + ",";
                                 _nextAddress++;
                                 size++;
                             }


                          }
                         _log.AppendLine(string.Format("ceccommands:{0}, Address:{1}", _ceccmds,_cmdstartaddress));
                         _dacMemoryMap.TryGetValue(_nextAddress, out _tempdata);
                         irdatasize = Conversion.ConvertBytetoInt(_tempdata);
                         _log.AppendLine(string.Format("irdatasize:{0}, Address:{1}", irdatasize.ToString(), _nextAddress.ToString()));
                         _nextAddress++;
                         size++;

                         if (cecpriority == 1)
                         {
                             _dacMemoryMap.TryGetValue(_nextAddress, out _tempdata);
                             irpriority = Conversion.ConvertBytetoInt(_tempdata);
                             _log.AppendLine(string.Format("irpriority:{0}, Address:{1}", irpriority.ToString(), _nextAddress.ToString()));
                             _nextAddress++;
                             size++;

                             _dacMemoryMap.TryGetValue(_nextAddress, out _tempdata);
                             ircodesetcount = Conversion.ConvertBytetoInt(_tempdata);
                             _log.AppendLine(string.Format("ircodesetcount:{0}, Address:{1}", ircodesetcount.ToString(), _nextAddress.ToString()));
                             _nextAddress++;
                             size++;

                             if (size > 0)
                             {
                                 _log.AppendLine(string.Format("ircodesets:{0}, Address:{1}", "", _nextAddress.ToString()));
                                 string _IdKeyId = string.Empty;
                                 string _timings = string.Empty;
                                 for (int k = 0; k < ircodesetcount; k++)
                                 {

                                     _IdKeyId += ConvertByteToId(GetData(3, _nextAddress)) + ";";
                                     //ircodesets.Add(ConvertByteToId(GetData(3, _nextAddress)));
                                     _nextAddress += 3;
                                     size += 3;
                                     string _tempData = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]).ToString("X");
                                     _IdKeyId += _tempData + ";";
                                     _nextAddress++;
                                     size++;
                                     _timings = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress)).ToString();
                                     _nextAddress += 2;
                                     size += 2;

                                 }
                                 ircodesets.Add(_IdKeyId);
                                 ircodesets.Add(_timings);
                             }

                             ipdatasize = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
                             _log.AppendLine(string.Format("ipdatasize:{0}, Address:{1}", ipdatasize.ToString(), _nextAddress.ToString()));
                             _nextAddress += 2;
                             size += 2;
                         }
                         else if (cecpriority == 2)
                         {
                             _dacMemoryMap.TryGetValue(_nextAddress, out _tempdata);
                             irpriority = Conversion.ConvertBytetoInt(_tempdata);
                             _log.AppendLine(string.Format("irpriority:{0}, Address:{1}", irpriority.ToString(), _nextAddress.ToString()));
                             _nextAddress++;
                             size++;

                             _dacMemoryMap.TryGetValue(_nextAddress, out _tempdata);
                             ircodesetcount = Conversion.ConvertBytetoInt(_tempdata);
                             _log.AppendLine(string.Format("ircodesetcount:{0}, Address:{1}", ircodesetcount.ToString(), _nextAddress.ToString()));
                             _nextAddress++;
                             size++;

                             int codesetbytesize = ircodesetcount * 3;

                             _log.AppendLine(string.Format("ircodesets:{0}, Address:{1}", "", _nextAddress.ToString()));
                             string _IdKeyId = string.Empty;
                             string _timings = string.Empty;
                             for (int k = 0; k < ircodesetcount; k++)
                             {

                                 _IdKeyId += ConvertByteToId(GetData(3, _nextAddress)) + ";";
                                 //ircodesets.Add(ConvertByteToId(GetData(3, _nextAddress)));
                                 _nextAddress += 3;
                                 size += 3;
                                 string _tempData = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]).ToString("X");
                                 _IdKeyId += _tempData + ";";
                                 _nextAddress++;
                                 size++;
                                 _timings = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress)).ToString();
                                 _nextAddress += 2;
                                 size += 2;

                             }
                             ircodesets.Add(_IdKeyId);
                             ircodesets.Add(_timings);
                             ipdatasize = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
                             _log.AppendLine(string.Format("ipdatasize:{0}, Address:{1}", ipdatasize.ToString(), _nextAddress.ToString()));
                             _nextAddress += 2;
                             size += 2;
                         }

                         //_nextAddress++;
                         //size++;
                     }
                     else
                     {
                         _dacMemoryMap.TryGetValue(_nextAddress, out _tempdata);
                         ceccmdcount = Conversion.ConvertBytetoInt(_tempdata);
                         _log.AppendLine(string.Format("ceccmdcount:{0}, Address:{1}", ceccmdcount.ToString(), _nextAddress.ToString()));
                         //ceccmdcount = Conversion.ConvertByteArrayToInt16(GetData(1, _nextAddress));
                         _nextAddress++;
                         size++;

                         _dacMemoryMap.TryGetValue(_nextAddress, out _tempdata);
                         irdatasize=Conversion.ConvertBytetoInt(_tempdata);
                         _log.AppendLine(string.Format("irdatasize:{0}, Address:{1}", irdatasize.ToString(), _nextAddress.ToString()));
                         _nextAddress++;
                         size++;
                         _dacMemoryMap.TryGetValue(_nextAddress, out _tempdata);
                         irpriority = Conversion.ConvertBytetoInt(_tempdata);
                         _log.AppendLine(string.Format("irpriority:{0}, Address:{1}", irpriority.ToString(), _nextAddress.ToString()));
                         _nextAddress++;
                         size++;
                         _dacMemoryMap.TryGetValue(_nextAddress, out _tempdata);
                         ircodesetcount = Conversion.ConvertBytetoInt(_tempdata);
                         _log.AppendLine(string.Format("ircodesetcount:{0}, Address:{1}", ircodesetcount.ToString(), _nextAddress.ToString()));
                         _nextAddress++;
                         size++;
                         int codesetbytesize=ircodesetcount*3;

                         _log.AppendLine(string.Format("ircodesets:{0}, Address:{1}", "", _nextAddress.ToString()));
                         string _timings = string.Empty;
                         string _IdKeyId = string.Empty;
                         
                         for (int k = 0; k < ircodesetcount; k++)
                         {

                             _IdKeyId += ConvertByteToId(GetData(3, _nextAddress)) + ";";
                             //ircodesets.Add(ConvertByteToId(GetData(3, _nextAddress)));
                             _nextAddress += 3;
                             size += 3;
                             string _tempData = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]).ToString("X");
                             _IdKeyId += _tempData + ";";
                             _nextAddress++;
                             size++;
                             _timings = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress)).ToString();
                             _nextAddress += 2;
                             size += 2;

                         }
                         ircodesets.Add(_IdKeyId);
                         ircodesets.Add(_timings);
                         
                         ipdatasize = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
                         _log.AppendLine(string.Format("ipdatasize:{0}, Address:{1}", ipdatasize.ToString(), _nextAddress.ToString()));
                         _nextAddress += 2;
                         size += 2;
                     }

                     
                     string cmds = string.Empty;
                     foreach (int _cmd in ceccmds)
                     {
                         cmds += "0x"+_cmd.ToString("X") + ",";
                     }

                     string _ids = string.Empty;
                     foreach (string id in ircodesets)
                     {
                         _ids += id + ",";
                     }

                     string fnid = "0x"+_ueifnId.ToString("X");

                     DataRow _dr = _dt.NewRow();
                     string[] _newrow = { fnid.ToString(), cecdatasize.ToString(), cecpriority.ToString(), ceccmdcount.ToString(), cmds, irdatasize.ToString(), irpriority.ToString(), ircodesetcount.ToString(), _ids, ipdatasize.ToString() };

                     _dt.Rows.Add(_newrow);
                     
                     keymapcounter++;
                 }

                // string _data = string.Format("{0},{1},{2},{3},{4},{5},{6},{7},{8},{9}", _ueifnId.ToString(), cecdatasize.ToString(), cecpriority.ToString(), ceccmds.Count.ToString(), irdatasize.ToString(), irpriority.ToString(), ircodesetcount.ToString(), ipdatasize.ToString());
            }

            System.IO.File.WriteAllText(@"C:\Working\Dac3aLog.txt", _log.ToString());
            //string _data=(string.Format("Devide :{0}, Size:{1} Brand:{2} KeyMapCount:{3}", _device, sizeofentry.ToString(),_brandName,_keyMapCount.ToString()));
        }

        public string ConvertByteToId(byte[] data)
        {
            string _id = string.Empty;
            byte[] _setupcode = new byte[2];

            if (data.Length != 3)
            {
                //MessageBox.Show("Invalid setup code");
            }
            else
            {
                byte[] _temp = new byte[1];
                _temp[0] = data[2];
                _id = Conversion.ConvertByteToString(_temp);
                _setupcode[0] = data[0];
                _setupcode[1] = data[1];
                int _code = Conversion.ConvertByteArrayToInt16(_setupcode);

                _id += ResetSetupCode(_code);
            }


            return _id;
        }

        public string ResetSetupCode(int data)
        {
            string _result = string.Empty;
            int len = 4 - data.ToString().Length;

            for (int i = len; i > 0; i--)
            {
                _result += "0";
            }
            _result += data.ToString();
            return _result;
        }

        private void SetDataTable()
        {
            _dt = new DataTable();
            _dt.Columns.Add("UEIFnId");
            _dt.Columns.Add("CECDataSize");
            _dt.Columns.Add("CECPriority");
            _dt.Columns.Add("CECCommandCount");
            _dt.Columns.Add("CECCommands");
            _dt.Columns.Add("IRDataSize");
            _dt.Columns.Add("IRPriority");
            _dt.Columns.Add("IRCodesetCount");
            _dt.Columns.Add("IRCodesets");
            _dt.Columns.Add("IPDataSize");
        }

        public void GetAllIpData()
        {
            string _entrydatasize = string.Empty;
            string _manufacturer = string.Empty;
            string _url = string.Empty;
            string _friendlyname = string.Empty;
            string _modelname = string.Empty;
            string _modelnumber = string.Empty;
            string _modeldesc = string.Empty;
            string _brand = string.Empty;
            string _ueidevtype = string.Empty;
            string _codesets = string.Empty;
            string _ardkey = string.Empty;
            SetIpDiscoverDt();
            int _adr=0;
            _toc.TryGetValue("IP", out _adr);
            _nextAddress = _adr;

            _nextAddress += 2;
            Int32 memorysize = Conversion.ConvertByteArrayToInt32(GetData(4, _nextAddress));
            _nextAddress += 4;
            int totalrecs = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
            _nextAddress += 2;

            for (int i = 0; i < totalrecs; i++)
            {
                int totalbytesize = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
                _nextAddress += 2;
                _entrydatasize = totalbytesize.ToString();

                int size = Conversion.ConvertBytetoInt(GetData(1,_nextAddress)[0]);
                _nextAddress += 1;
                if (size > 0)
                {
                    _manufacturer = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                    _nextAddress += size;
                }

                size = size = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]);
                _nextAddress += 1;
                if (size > 0)
                {
                    _url = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                    _nextAddress += size;
                }

                size = size = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]);
                _nextAddress += 1;
                if (size > 0)
                {
                    _friendlyname = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                    _nextAddress += size;
                }

                size = size = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]);
                _nextAddress += 1;
                if (size > 0)
                {
                    _modelname = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                    _nextAddress += size;
                }

                size = size = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]);
                _nextAddress += 1;
                if (size > 0)
                {
                    _modelnumber = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                    _nextAddress += size;
                }

                size = size = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]);
                _nextAddress += 1;
                if (size > 0)
                {
                    _modeldesc = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                    _nextAddress += size;
                }

                size = 2;
                //_nextAddress += 2;
                if (size > 0)
                {
                    int brandid = Conversion.ConvertByteArrayToInt16(GetData(size, _nextAddress));
                    _brand = _brands[brandid];
                    _nextAddress += size;
                }

                size = 1;
                //_nextAddress += 1;
                if (size > 0)
                {
                    _ueidevtype = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                    _nextAddress += size;
                }

                size = size = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]);
                _nextAddress += 1;
                if (size > 0)
                {
                    //int _nocodesets = size / 3;
                    int _nocodesets = size;//Changed after the rev0.11
                    for (int j = 0; j < _nocodesets; j++)
                    {
                        string region = Conversion.ConvertByteArrayToUTF8(GetData(4, _nextAddress))+":";
                        _nextAddress += 4;
                        _codesets += region+ConvertByteToId(GetData(3, _nextAddress)) + ";";
                        _nextAddress += 3;
                    }

                   
                }
                byte[] ardData=GetData(4, _nextAddress);
                _ardkey="0x";
                for (int k = 3; k >= 0; k--)
                {
                    _ardkey += ardData[k].ToString("X");
                }


                //_ardkey = "0x" + Convert.ToInt32(_arddata.ToString(), 16);
                _nextAddress += 4;

                string[] data = { _entrydatasize, _manufacturer, _url, _friendlyname, _modelname, _modelnumber, _modeldesc, _brand, _ueidevtype, _codesets, _ardkey };
                IpDiscoverData.Rows.Add(data);

                 _entrydatasize = string.Empty;
                 _manufacturer = string.Empty;
                 _url = string.Empty;
                 _friendlyname = string.Empty;
                 _modelname = string.Empty;
                 _modelnumber = string.Empty;
                 _modeldesc = string.Empty;
                 _brand = string.Empty;
                 _ueidevtype = string.Empty;
                 _codesets = string.Empty;
                 _ardkey = string.Empty;

            }

        }

        public void GetAllCecData()
        {
            string _entrydatasize = string.Empty;
            string _ueidevtype = string.Empty;
            string _vendorid = string.Empty;
            string _osdstring = string.Empty;
            string _manufacturerid = string.Empty;
            string _brand = string.Empty;
            string _codesets = string.Empty;
            string _ardkey = string.Empty;
            SetCecDiscoverDt();
            int _adr = 0;
            _toc.TryGetValue("CE", out _adr);
            _nextAddress = _adr;

            _nextAddress += 2;
            Int32 memorysize = Conversion.ConvertByteArrayToInt32(GetData(4, _nextAddress));
            _nextAddress += 4;
            int totalrecs = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
            _nextAddress += 2;

            for (int i = 0; i < totalrecs; i++)
            {
                int totalbytesize = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
                _nextAddress += 2;
                _entrydatasize = totalbytesize.ToString();

                int size = 1;
                //_nextAddress += 1;
                if (size > 0)
                {
                    _ueidevtype = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                    _nextAddress += size;
                }

                size = 2;
                //_nextAddress += 1;
                if (size > 0)
                {
                    byte[] ardDa = GetData(size, _nextAddress);
                    //_ardkey = "0x";
                    for (int k = 1; k >= 0; k--)
                    {
                        //_ardkey += ardData[k].ToString("X");
                        _manufacturerid += ardDa[k].ToString("X") + " ";

                    }
                    //_manufacturerid = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                    _nextAddress += size;
                }

                size = 3;
                //_nextAddress += 1;
                if (size > 0)
                {
                    byte[] ardData = GetData(size, _nextAddress);
                    //_ardkey = "0x";
                    for (int k = 2; k >= 0; k--)
                    {
                        //_ardkey += ardData[k].ToString("X");
                        _vendorid += ardData[k].ToString("X") + " ";

                    }
                    //_vendorid = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                    _nextAddress += size;
                }

                size = size = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]);
                _nextAddress += 1;
                if (size > 0)
                {
                    _osdstring = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                    _nextAddress += size;
                }

                

                size = 2;
                //_nextAddress += 2;
                if (size > 0)
                {
                    int brandid = Conversion.ConvertByteArrayToInt16(GetData(size, _nextAddress));
                    _brand = _brands[brandid];
                    _nextAddress += size;
                }

                
                size = size = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]);
                _nextAddress += 1;
                if (size > 0)
                {
                    //int _nocodesets = size / 3;
                    int _nocodesets = size; //changed based on the spec rev 0.11
                    for (int j = 0; j < _nocodesets; j++)
                    {
                        string _region = Conversion.ConvertByteArrayToUTF8(GetData(4, _nextAddress))+":";
                        _nextAddress += 4;
                        
                        _codesets +=_region+ConvertByteToId(GetData(3, _nextAddress)) + ";";
                        _nextAddress += 3;
                    }


                }
                byte[] ardD = GetData(4, _nextAddress);
                _ardkey = "0x";
                for (int k = 3; k >= 0; k--)
                {
                    _ardkey += ardD[k].ToString("X");
                }


                //_ardkey = "0x" + Convert.ToInt32(_arddata.ToString(), 16);
                _nextAddress += 4;

                string[] data = { _entrydatasize, _ueidevtype,_manufacturerid, _vendorid, _osdstring, _brand, _codesets, _ardkey };
                CecDiscoverdata.Rows.Add(data);

                _entrydatasize = string.Empty;
                _ueidevtype = string.Empty;
                _vendorid = string.Empty;
                _osdstring = string.Empty;
                _manufacturerid = string.Empty;
                _brand = string.Empty;
                _codesets = string.Empty;
                _ardkey = string.Empty;

            }

        }

        public void GetAllInfoFrameData()
        {
            string _entrydatasize = string.Empty;
            string _ueidevtype = string.Empty;
            string _infoframesize = string.Empty;
            string _brand = string.Empty;
            string _codesets = string.Empty;
            string _ardkey = string.Empty;
            SetInfoFrameRawDt();
            int _adr = 0;
            _toc.TryGetValue("IF", out _adr);
            _nextAddress = _adr;

            _nextAddress += 2;
            Int32 memorysize = Conversion.ConvertByteArrayToInt32(GetData(4, _nextAddress));
            _nextAddress += 4;
            int totalrecs = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
            _nextAddress += 2;

            for (int i = 0; i < totalrecs; i++)
            {
                int totalbytesize = Conversion.ConvertByteArrayToInt16(GetData(2, _nextAddress));
                _nextAddress += 2;
                _entrydatasize = totalbytesize.ToString();

                int size = 1;
                //_nextAddress += 1;
                if (size > 0)
                {
                    _ueidevtype = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                    _nextAddress += size;
                }

                size = 2;
                //_nextAddress += 2;
                if (size > 0)
                {
                    int brandid = Conversion.ConvertByteArrayToInt16(GetData(size, _nextAddress));
                    _brand = _brands[brandid];
                    _nextAddress += size;
                }

                size = 1;
                int rawdatasize = 0;
                if (size > 0)
                {
                    rawdatasize = Conversion.ConvertBytetoInt(GetData(size, _nextAddress)[0]);
                    _nextAddress += size;
                }

                string _rawdata = string.Empty;
                if (rawdatasize > 0)
                {
                    _rawdata = ConvertHexToString((GetData(rawdatasize, _nextAddress)));
                    _nextAddress += rawdatasize;
                }

                size = 1;
                int frametype = 0;
                if (size > 0)
                {
                    frametype = Conversion.ConvertBytetoInt(GetData(size, _nextAddress)[0]);
                    _nextAddress += size;
                }

                size = 1;
                int InfoFieldCount = 0;
                if (size > 0)
                {
                    InfoFieldCount = Conversion.ConvertBytetoInt(GetData(size, _nextAddress)[0]);
                    _nextAddress += size;
                }
                string field1 = string.Empty;
                string field2 = string.Empty;
                if (frametype < 255)
                {
                    size = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]);
                    _nextAddress++;

                    if (size > 0)
                    {
                        field1 = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                        _nextAddress += size;
                    }

                    size = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]);
                    _nextAddress++;

                    if (size > 0)
                    {
                        field2 = Conversion.ConvertByteArrayToUTF8(GetData(size, _nextAddress));
                        _nextAddress += size;
                    }
                }
                else
                {
                    field1 = "";
                    field2 = "";
                    //size = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]);
                    //_nextAddress++;
                    
                    //_codesets = "";
                }

                string region = string.Empty;
                    size = Conversion.ConvertBytetoInt(GetData(1, _nextAddress)[0]);
                    _nextAddress += 1;
                    if (size > 0)
                    {
                        //int _nocodesets = size / 3;
                        int _nocodesets = size;

                        for (int j = 0; j < _nocodesets; j++)
                        {
                            region = Conversion.ConvertByteArrayToUTF8(GetData(4, _nextAddress))+":";
                            _nextAddress += 4;
                            _codesets += region + ConvertByteToId(GetData(3, _nextAddress)) + ";";
                            _nextAddress += 3;
                        }


                    }
                
                
                byte[] ardD = GetData(4, _nextAddress);
                _ardkey = "0x";
                for (int k = 3; k >= 0; k--)
                {
                    _ardkey +=CorrectArdKey(ardD[k].ToString("X"));
                }


                //_ardkey = "0x" + Convert.ToInt32(_arddata.ToString(), 16);
                _nextAddress += 4;

                string[] data = { _entrydatasize, _ueidevtype, _brand, rawdatasize.ToString(),_rawdata, frametype.ToString(), InfoFieldCount.ToString(), field1, field2, _codesets, _ardkey };
                Infoframedata.Rows.Add(data);

                _entrydatasize = string.Empty;
                _ueidevtype = string.Empty;
                _rawdata = string.Empty;
                _brand = string.Empty;
                _codesets = string.Empty;
                _ardkey = string.Empty;

            }
        }

        private string CorrectArdKey(string data)
        {
            string _correctedData = string.Empty;

            int len = 2 - data.Length;
            for (int i = 0; i < len; i++)
            {
                _correctedData += "0";
            }

            _correctedData += data;

            return _correctedData;
        }

        private string ConvertHexToString(byte[] data)
        {
            string _data = string.Empty;

            foreach (byte by in data)
            {
                _data += by.ToString("X") + " ";
            }

            return _data;
        }

        public byte[] ReverseArray(byte[] data)
        {
            Array.Reverse(data);
            return data;
        }



        public DataTable IpDiscoverData = new DataTable();
        public DataTable CecDiscoverdata = new DataTable();
        public DataTable Infoframedata = new DataTable();

        private void SetIpDiscoverDt()
        {
            IpDiscoverData = new DataTable();
            IpDiscoverData.Columns.Add("EntryDataSize");
            IpDiscoverData.Columns.Add("Manufacturer");
            IpDiscoverData.Columns.Add("URL");
            IpDiscoverData.Columns.Add("Friendly Name");
            IpDiscoverData.Columns.Add("Model Name");
            IpDiscoverData.Columns.Add("Model Number");
            IpDiscoverData.Columns.Add("Model Description");
            IpDiscoverData.Columns.Add("Brand Id Field");
            IpDiscoverData.Columns.Add("UEI Device Type Id");
            IpDiscoverData.Columns.Add("Codesets");
            IpDiscoverData.Columns.Add("ARD Key");
            
        }
        private void SetCecDiscoverDt()
        {
            CecDiscoverdata = new DataTable();
            CecDiscoverdata.Columns.Add("EntryDataSize");
            CecDiscoverdata.Columns.Add("UEI Device Type Id");
            CecDiscoverdata.Columns.Add("EDID Manufacturer Id");
            CecDiscoverdata.Columns.Add("CEC Vendor Id");
            CecDiscoverdata.Columns.Add("CEC OSD string");
            CecDiscoverdata.Columns.Add("Brand");
            CecDiscoverdata.Columns.Add("Codeset");
            CecDiscoverdata.Columns.Add("ARD Key");
        }
        private void SetInfoFrameRawDt()
        {
            Infoframedata = new DataTable();
            Infoframedata.Columns.Add("EntryDataSize");
            Infoframedata.Columns.Add("UEI Device Type Id");
            Infoframedata.Columns.Add("Brand");
            Infoframedata.Columns.Add("RawDataSize");
            Infoframedata.Columns.Add("InfoFrameRawData");
            Infoframedata.Columns.Add("InfoFrameType");
            Infoframedata.Columns.Add("InfoFrameFieldsCount");
            Infoframedata.Columns.Add("SIF Name");
            Infoframedata.Columns.Add("SIF Description");
            Infoframedata.Columns.Add("Codeset");
            Infoframedata.Columns.Add("ARD Key");
        }

        public int ARDRecordSize = 0;
        public int ARDRecordTOCHeaderSize = 0;
        public int LastArdAddress = 0;
        

        
    }

    
}
